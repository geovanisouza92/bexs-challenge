# BEXS Challenge

Coding challenge for BEXS selection process.

## Installing / getting started

You need to [install Go](https://golang.org/doc/install). The project was made with Go 1.15, but probably works fine with Go 1.0 onwards.

After that, clone the project and you can run it right away:

```
$ git clone https://gitlab.com/geovanisouza92/bexs-challenge.git
$ cd bexs-challenge
```

## CLI interface

To start an REPL-like interface, run:

```
$ go run main.go input-routes.csv
```

## HTTP / REST interface

To start a HTTP server, run:

```
$ go run main.go -s input-routes.csv
```

The REST endpoints accepts and responds with `application/json` and will be available at port `8080` on your machine.

To register a new Route segment, execute (filling the appropriate values):

```
$ curl -X POST http://localhost:8080/route --data '{"from":__string__,"to":__string__,"cost":__number__}'
```

To search a Route, execute (filling the appropriate values):

```
$ curl http://localhost:8080/route?from=__string__&to=__string__
```

## Architecture

It's basically KISS. Single file (`main.go`) with two interfaces.

The main type is called `Graph`, but besides its name, it's just a mapping of origins to destinations and respective costs.

The main algorithm (`Graph#SearchRoute`) is a simple recursive reducer based on dynamic programming. It compares the route between each intermediate destination and the final destination. If the path of one intermediate destination results in a cheaper trip, it is replaced and the search continues.

It will probably not scale well with large graphs (thousands of routes segments), but I didn't benchmarked it.

There's room for performance improvement by passing a "cache" to `SearchRoute` in order to skip recalculation of known routes. Ex. if I need to search all the options between `BRC` and `CDG`, the function will reach `BRC - SCL - ORL - CDG`, but if another path, like `BRC - ORL - CDG` is available, the segment `ORL - CDG` will be calculated twice.

For this simple example it doesn't seems a problem, but in large instances all this duplicated calculations could hurt performance.
