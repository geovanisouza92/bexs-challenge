package main

import (
	"bufio"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"log"
	"math"
	"net/http"
	"os"
	"strconv"
	"strings"
)

type Place string

type Cost int

type Destination struct {
	place Place
	cost  Cost
}

type Graph map[Place][]Destination

func NewGraphFromFile(file io.ReadWriter) Graph {
	g := make(Graph)

	if file != nil {
		scanner := bufio.NewScanner(file)
		for scanner.Scan() {
			parts := strings.Split(scanner.Text(), ",")
			if len(parts) < 3 {
				continue
			}

			from := Place(parts[0])
			to := Place(parts[1])
			if cost, err := strconv.Atoi(parts[2]); err != nil {
				panic(err)
			} else {
				g.AddSegment(from, to, Cost(cost))
			}
		}
	}

	return g
}

func (g Graph) Persist(file io.ReadWriter) error {
	if file == nil {
		return nil
	}

	var b strings.Builder
	for place, destinations := range g {
		for _, destination := range destinations {
			fmt.Fprintf(&b, "%s,%s,%d\n", place, destination.place, destination.cost)
		}
	}

	_, err := fmt.Fprint(file, b.String())
	return err
}

func (g Graph) AddSegment(from Place, to Place, cost Cost) {
	if _, exists := g[from]; !exists {
		g[from] = []Destination{}
	}
	g[from] = append(g[from], Destination{to, cost})
}

type Route struct {
	connections []Place
	totalCost   Cost
}

func (r Route) Empty() bool {
	return len(r.connections) == 0
}

func (r Route) String() string {
	s := make([]string, len(r.connections))
	for i, p := range r.connections {
		s[i] = string(p)
	}
	return fmt.Sprintf("%s > $%d", strings.Join([]string(s), " - "), r.totalCost)
}

func (g Graph) SearchRoute(from Place, to Place) Route {
	if from == to {
		return Route{[]Place{to}, 0}
	}

	best := Route{[]Place{}, math.MaxInt64}
	for _, destination := range g[from] {
		stop := g.SearchRoute(destination.place, to)
		current := Route{
			connections: append([]Place{from}, stop.connections...),
			totalCost:   destination.cost + stop.totalCost,
		}
		if current.totalCost < best.totalCost {
			best = current
		}
	}

	if best.Empty() {
		best.totalCost = 0
	}

	return best
}

func restHandlerFactory(g Graph, file io.ReadWriter) http.HandlerFunc {
	respondWith := func(res http.ResponseWriter, status int, body interface{}) {
		res.WriteHeader(status)
		e := json.NewEncoder(res)
		if err := e.Encode(body); err != nil {
			fmt.Printf("Error while writing response: %v", err)
		}
	}

	readBody := func(req *http.Request) map[string]interface{} {
		b := map[string]interface{}{}
		d := json.NewDecoder(req.Body)
		if err := d.Decode(&b); err != nil {
			fmt.Printf("Error while reading request: %v", err)
		}
		return b
	}

	return func(res http.ResponseWriter, req *http.Request) {
		if req.URL.Path == "/route" {
			if req.Method == http.MethodPost {
				body := readBody(req)
				if body["from"] == "" || body["to"] == "" {
					respondWith(res, http.StatusBadRequest, map[string]interface{}{
						"message": "Invalid parameters, you should provide `from` and `to` to add a new segment",
					})
					return
				}
				cost, ok := body["cost"].(float64)
				if !ok {
					respondWith(res, http.StatusBadRequest, map[string]interface{}{
						"message": "Invalid parameter `cost` is missing or has an invalid format",
					})
					return
				}

				g.AddSegment(Place(body["from"].(string)), Place(body["to"].(string)), Cost(cost))
				if err := g.Persist(file); err != nil {
					respondWith(res, http.StatusInternalServerError, map[string]interface{}{
						"message": err.Error(),
					})
				} else {
					respondWith(res, http.StatusCreated, map[string]interface{}{
						"message": fmt.Sprintf("Route from %q to %q was created", body["from"], body["to"]),
					})
				}

				return
			}

			q := req.URL.Query()
			from := q.Get("from")
			to := q.Get("to")
			if from == "" || to == "" {
				respondWith(res, http.StatusBadRequest, map[string]interface{}{
					"message": "Invalid parameters, you should provide `from` and `to` to add a new segment",
				})
				return
			}

			r := g.SearchRoute(Place(from), Place(to))
			if r.Empty() {
				respondWith(res, http.StatusNotFound, map[string]interface{}{
					"message": fmt.Sprintf("No route found from %q to %q", from, to),
				})
			} else {
				respondWith(res, http.StatusOK, map[string]interface{}{
					"message": fmt.Sprintf("Best route: %s", r),
				})
			}
		} else {
			respondWith(res, http.StatusOK, map[string]interface{}{
				"message": []string{
					"Usage:",
					"POST /route   to add a new route segment",
					"  Body should include a json object like {\"from\":\"string\",\"to\":\"string\",\"cost\":number}",
					"GET /route?from=(string)&to=(string)   to get the best route between two places",
				},
			})
		}
	}
}

func startHTTPInterface(file io.ReadWriter) {
	g := NewGraphFromFile(file)

	http.HandleFunc("/", restHandlerFactory(g, file))
	fmt.Println("Listening at :8080")
	log.Fatal(http.ListenAndServe(":8080", nil))
}

func startREPLInterface(file io.ReadWriter, in io.Reader, out io.Writer) {
	g := NewGraphFromFile(file)

	fmt.Fprint(out, "Please, enter the route: ")
	scanner := bufio.NewScanner(in)
	for scanner.Scan() {
		command := scanner.Text()
		if command == "" || command == "exit" {
			fmt.Fprintln(out, "Bye!")
			return
		}

		places := strings.Split(command, "-")
		if len(places) != 2 {
			fmt.Fprintf(out, "Usage: Inform two separate places separated by hyphen. Ex: GRU-CDG\nPlease, enter the route: ") // TODO Usage
			continue
		}

		r := g.SearchRoute(Place(places[0]), Place(places[1]))
		if r.Empty() {
			fmt.Fprintf(out, "No route found from %q to %q\nPlease, enter the route: ", places[0], places[1])
			continue
		}

		fmt.Fprintf(out, "Best route: %s\nPlease, enter the route: ", r)
	}
}

var startServer = flag.Bool("s", false, "Start HTTP server")

func main() {
	flag.Parse()

	if file, err := os.OpenFile(flag.Arg(0), os.O_RDWR|os.O_CREATE, 0644); err != nil {
		log.Fatal(err)
	} else {
		if *startServer {
			startHTTPInterface(file)
		} else {
			startREPLInterface(file, os.Stdin, os.Stdout)
		}
	}
}
