package main

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestShouldCreateANewGraph(t *testing.T) {
	var file bytes.Buffer
	file.WriteString("GRU,CDG,75\n")

	g := NewGraphFromFile(&file)

	if len(g) != 1 {
		t.Errorf("incorrect destinations mapping: got %v want %v", len(g), 1)
	}

	dest := g["GRU"][0]

	if dest.place != Place("CDG") {
		t.Errorf("incorrect parsing of destination: got %v want %v", dest.place, "CDG")
	}
	if dest.cost != 75 {
		t.Errorf("incorrect parsing of cost: got %v want %v", dest.cost, 75)
	}
}

func TestGraphShouldFindAValidRoute(t *testing.T) {
	g := NewGraphFromFile(nil)
	g.AddSegment("GRU", "BRC", 10)
	g.AddSegment("BRC", "SCL", 5)
	g.AddSegment("GRU", "CDG", 75)
	g.AddSegment("GRU", "SCL", 20)
	g.AddSegment("GRU", "ORL", 56)
	g.AddSegment("ORL", "CDG", 5)
	g.AddSegment("SCL", "ORL", 20)

	r := g.SearchRoute("GRU", "CDG")

	if r.totalCost != 40 {
		t.Errorf("Expected route total cost to be $40, but got %d", r.totalCost)
		t.Errorf("%s", r)
	}
}

func TestGraphShouldHandleImpossibleRoutes(t *testing.T) {
	g := NewGraphFromFile(nil)
	g.AddSegment("GRU", "BRC", 10)
	g.AddSegment("BRC", "SCL", 5)
	g.AddSegment("GRU", "CDG", 75)
	g.AddSegment("GRU", "SCL", 20)
	g.AddSegment("GRU", "ORL", 56)
	g.AddSegment("ORL", "CDG", 5)
	g.AddSegment("SCL", "ORL", 20)

	r := g.SearchRoute("CDG", "GRU")

	if !r.Empty() {
		t.Errorf("valid route found when none should exist: got %v", r)
	}

	if r.totalCost != 0 {
		t.Errorf("Expected route total cost to be $0, but got %d", r.totalCost)
	}
}

func TestHTTPShouldReturnUsageInfo(t *testing.T) {
	var file bytes.Buffer

	g := NewGraphFromFile(&file)
	req, err := http.NewRequest("GET", "/", nil)
	if err != nil {
		t.Error(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(restHandlerFactory(g, &file))

	handler.ServeHTTP(rr, req)

	if rr.Code != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v", rr.Code, http.StatusOK)
	}

	expected := "{\"message\":[\"Usage:\",\"POST /route   to add a new route segment\",\"  Body should include a json object like {\\\"from\\\":\\\"string\\\",\\\"to\\\":\\\"string\\\",\\\"cost\\\":number}\",\"GET /route?from=(string)\\u0026to=(string)   to get the best route between two places\"]}\n"
	if rr.Body.String() != expected {
		t.Errorf("handler returned unexpected body: got %v want %v", rr.Body.String(), expected)
	}
}

func TestHTTPShouldRegisterANewRouteSegment(t *testing.T) {
	var file bytes.Buffer
	g := NewGraphFromFile(&file)
	var body bytes.Buffer
	json.NewEncoder(&body).Encode(map[string]interface{}{"from": "GRU", "to": "CDG", "cost": 75})

	req, err := http.NewRequest("POST", "/route", &body)
	if err != nil {
		t.Error(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(restHandlerFactory(g, &file))

	handler.ServeHTTP(rr, req)

	if rr.Code != http.StatusCreated {
		t.Errorf("handler returned wrong status code: got %v want %v", rr.Code, http.StatusCreated)
	}

	expected := "{\"message\":\"Route from \\\"GRU\\\" to \\\"CDG\\\" was created\"}\n"
	if rr.Body.String() != expected {
		t.Errorf("handler returned unexpected body: got %v want %v", rr.Body.String(), expected)
	}

	expected = "GRU,CDG,75\n"
	if file.String() != expected {
		t.Errorf("handler did not persisted the correct content: got %v want %v", file.String(), expected)
	}
}

func TestHTTPShouldValidateBodyParameters(t *testing.T) {
	g := NewGraphFromFile(nil)
	var body bytes.Buffer
	json.NewEncoder(&body).Encode(map[string]interface{}{"from": "", "to": "", "cost": 75})

	req, err := http.NewRequest("POST", "/route", &body)
	if err != nil {
		t.Error(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(restHandlerFactory(g, nil))

	handler.ServeHTTP(rr, req)

	if rr.Code != http.StatusBadRequest {
		t.Errorf("handler returned wrong status code: got %v want %v", rr.Code, http.StatusBadRequest)
	}

	expected := "{\"message\":\"Invalid parameters, you should provide `from` and `to` to add a new segment\"}\n"
	if rr.Body.String() != expected {
		t.Errorf("handler did not responded with correct content: got %v want %v", rr.Body.String(), expected)
	}
}

func TestHTTPShouldValidateQueryParameters(t *testing.T) {
	g := NewGraphFromFile(nil)

	req, err := http.NewRequest("GET", "/route?from=&to=", nil)
	if err != nil {
		t.Error(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(restHandlerFactory(g, nil))

	handler.ServeHTTP(rr, req)

	if rr.Code != http.StatusBadRequest {
		t.Errorf("handler returned wrong status code: got %v want %v", rr.Code, http.StatusBadRequest)
	}

	expected := "{\"message\":\"Invalid parameters, you should provide `from` and `to` to add a new segment\"}\n"
	if rr.Body.String() != expected {
		t.Errorf("handler did not responded with correct content: got %v want %v", rr.Body.String(), expected)
	}
}

func TestHTTPShouldValidateCostParameter(t *testing.T) {
	g := NewGraphFromFile(nil)
	var body bytes.Buffer
	json.NewEncoder(&body).Encode(map[string]interface{}{"from": "GRU", "to": "CDG", "cost": "blah"})

	req, err := http.NewRequest("POST", "/route", &body)
	if err != nil {
		t.Error(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(restHandlerFactory(g, nil))

	handler.ServeHTTP(rr, req)

	if rr.Code != http.StatusBadRequest {
		t.Errorf("handler returned wrong status code: got %v want %v", rr.Code, http.StatusBadRequest)
	}

	expected := "{\"message\":\"Invalid parameter `cost` is missing or has an invalid format\"}\n"
	if rr.Body.String() != expected {
		t.Errorf("handler did not responded with correct content: got %v want %v", rr.Body.String(), expected)
	}
}

func TestHTTPShouldFindAValidRoute(t *testing.T) {
	g := NewGraphFromFile(nil)
	g.AddSegment("GRU", "BRC", 10)
	g.AddSegment("BRC", "SCL", 5)
	g.AddSegment("GRU", "CDG", 75)
	g.AddSegment("GRU", "SCL", 20)
	g.AddSegment("GRU", "ORL", 56)
	g.AddSegment("ORL", "CDG", 5)
	g.AddSegment("SCL", "ORL", 20)

	req, err := http.NewRequest("GET", "/route?from=GRU&to=CDG", nil)
	if err != nil {
		t.Error(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(restHandlerFactory(g, nil))

	handler.ServeHTTP(rr, req)

	if rr.Code != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v", rr.Code, http.StatusOK)
	}

	expected := "{\"message\":\"Best route: GRU - BRC - SCL - ORL - CDG \\u003e $40\"}\n"
	if rr.Body.String() != expected {
		t.Errorf("handler did not persisted the correct content: got %v want %v", rr.Body.String(), expected)
	}
}

func TestHTTPShouldHandleImpossibleRoute(t *testing.T) {
	g := NewGraphFromFile(nil)
	g.AddSegment("GRU", "BRC", 10)
	g.AddSegment("BRC", "SCL", 5)
	g.AddSegment("GRU", "CDG", 75)
	g.AddSegment("GRU", "SCL", 20)
	g.AddSegment("GRU", "ORL", 56)
	g.AddSegment("ORL", "CDG", 5)
	g.AddSegment("SCL", "ORL", 20)

	req, err := http.NewRequest("GET", "/route?from=CDG&to=GRU", nil)
	if err != nil {
		t.Error(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(restHandlerFactory(g, nil))

	handler.ServeHTTP(rr, req)

	if rr.Code != http.StatusNotFound {
		t.Errorf("handler returned wrong status code: got %v want %v", rr.Code, http.StatusNotFound)
	}

	expected := "{\"message\":\"No route found from \\\"CDG\\\" to \\\"GRU\\\"\"}\n"
	if rr.Body.String() != expected {
		t.Errorf("handler did not persisted the correct content: got %v want %v", rr.Body.String(), expected)
	}
}

func TestCLIShouldFindAValidRoute(t *testing.T) {
	var file bytes.Buffer
	file.WriteString("GRU,BRC,10\n")
	file.WriteString("BRC,SCL,5\n")
	file.WriteString("GRU,CDG,75\n")
	file.WriteString("GRU,SCL,20\n")
	file.WriteString("GRU,ORL,56\n")
	file.WriteString("ORL,CDG,5\n")
	file.WriteString("SCL,ORL,20\n")

	var in bytes.Buffer
	in.WriteString("GRU-CDG\n")

	var out bytes.Buffer

	startREPLInterface(&file, &in, &out)

	expected := "Please, enter the route: Best route: GRU - BRC - SCL - ORL - CDG > $40\nPlease, enter the route: "
	if out.String() != expected {
		t.Errorf("invalid output got %v want %v", out.String(), expected)
	}
}

func TestCLIShouldValidateCommand(t *testing.T) {
	var file bytes.Buffer
	file.WriteString("GRU,BRC,10\n")
	file.WriteString("BRC,SCL,5\n")
	file.WriteString("GRU,CDG,75\n")
	file.WriteString("GRU,SCL,20\n")
	file.WriteString("GRU,ORL,56\n")
	file.WriteString("ORL,CDG,5\n")
	file.WriteString("SCL,ORL,20\n")

	var in bytes.Buffer
	in.WriteString("GRU:CDG\nexit\n")

	var out bytes.Buffer

	startREPLInterface(&file, &in, &out)

	expected := "Please, enter the route: Usage: Inform two separate places separated by hyphen. Ex: GRU-CDG\nPlease, enter the route: Bye!\n"
	if out.String() != expected {
		t.Errorf("invalid output got %v want %v", out.String(), expected)
	}
}

func TestCLIShouldHandleImpossibleRoutes(t *testing.T) {
	var file bytes.Buffer
	file.WriteString("GRU,BRC,10\n")
	file.WriteString("BRC,SCL,5\n")
	file.WriteString("GRU,CDG,75\n")
	file.WriteString("GRU,SCL,20\n")
	file.WriteString("GRU,ORL,56\n")
	file.WriteString("ORL,CDG,5\n")
	file.WriteString("SCL,ORL,20\n")

	var in bytes.Buffer
	in.WriteString("CDG-GRU\n")

	var out bytes.Buffer

	startREPLInterface(&file, &in, &out)

	expected := "Please, enter the route: No route found from \"CDG\" to \"GRU\"\nPlease, enter the route: "
	if out.String() != expected {
		t.Errorf("invalid output got %v want %v", out.String(), expected)
	}
}
